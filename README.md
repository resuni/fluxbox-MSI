# fluxbox-MSI

This repository is mainly to document what I did to create this MSI-themed desktop style.

Fluxbox style is from https://github.com/cww/fluxbox-themes

Add the following to .fluxbox/startup to set the desktop background, start conky, and start xcompmgr:

> \# Applications you want to run with fluxbox.
>
> \# MAKE SURE THAT APPS THAT KEEP RUNNING HAVE AN ''&'' AT THE END.
>
> xcompmgr &
>
> feh --bg-center Pictures/blackred_msi.png &
>
> conky &

For compositing to work properly, it's also necessary to add the following to /etc/X11/xorg.conf:

> Section "Extensions"
>
>	Option	"Composite"	"enable"
>
>	Option	"RENDER"	"Enable"
>
> EndSection

My entire xorg.conf file will be uploaded for reference.

The following entries were made in ~/fluxbox/keys for launching sakura and dmenu
> \# Launch Sakura
>
> Mod4 Return :Exec sakura
>
> \# Launch dmenu
>
> Mod4 D :Exec dmenu_run -i -nb \\#131313 -nf \\#c0c0c0 -sb \\#774B46 -sf \\#c0c0c0

I'm particular about my mouse focus settings, so I make the following changes in ~/.fluxbox/init:

Line 37:
> session.screen0.autoRaise:      true

Line 57:

> session.screen0.focusModel:     MouseFocus

Using Metro for Steam. An associated custom.styles file will be uploaded. http://metroforsteam.com/

Note: I stopped using Metro for Steam since it breaks some of Steam's basic functionality. 

Suggested Firefox themes:
 - https://addons.mozilla.org/en-US/firefox/addon/msi-dragon-shield-2/
 - https://addons.mozilla.org/en-US/firefox/addon/msi-firefox-theme/

## Locations for files
 - ~/Pictures/blackred_msi.png
 - ~/.conkyrc
 - ~/.steam/steam/skins/custom.styles
 - ~/.lua/scripts/rings.lua
 - ~/.config/sakura/sakura.conf
 - /etc/X11/xorg.conf - For reference only. Do NOT overwrite existing xorg.conf.

## Credits
 - [cww/fluxbox-themes](https://github.com/cww/fluxbox-themes) repository, for the Fluxbox theme.
 - [DThought/conky-rings](https://github.com/DThought/conky-rings) repository, for giving me conky configs to start with. 
 - [/u/seal54321](http://redd.it/32dea3), for the original MSI wallpaper that I re-colored.
